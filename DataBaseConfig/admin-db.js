const admin = require('firebase-admin');
const config = require('../config');


const serviceAcount = require('../Firebase-Tools/bigoode-4f1db-firebase-adminsdk-2cdvr-35632ba2e0.json');

const adminDb = admin.initializeApp({
    credential: admin.credential.cert(serviceAcount),
    databaseURL: config.firebaseConfig.databaseURL
});
const db = adminDb.firestore()

module.exports = {
    adminDb,
    db
}

